# **What did we learn today? What activites did you do? What scenes have impressed you**
I learned the basic flow of the request-response model as well as the Restful API and Spring Boot today, of which the learning of Spring Boot impressed me the most and influenced me the most.
# **Pleas use one word to express your feelings about today's class.**
useful.
# **What do you think about this? What was the most meaningful aspect of this activity?**
I think the most useful part is the learning part of Spring Boot, which makes my development more handy.
# **Where do you most want to apply what you have learned today? What changes will you make?**
I would like to apply what I've learned today to my day-to-day development and understanding some of the request process.


https://gitlab.com/Hilton7/mars-rover-starter