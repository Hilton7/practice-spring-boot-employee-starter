package com.thoughtworks.springbootemployee;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.bean.Company;
import com.thoughtworks.springbootemployee.bean.Employee;
import com.thoughtworks.springbootemployee.service.CompanyService;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.annotation.Resource;

import static org.hamcrest.Matchers.hasSize;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyTest {
    private final Company COMPANY = new Company("boot");
    private final Employee EMPLOYEE = new Employee(null, "marry", 30, "woman", COMPANY, 800000);

    @BeforeEach
    public void setUp() {
        companyService.addCompany(COMPANY);
        employeeService.addEmployee(EMPLOYEE);
    }

    @AfterEach
    public void cleanUp() {
        companyService.clean();
        employeeService.clean();
    }

    @Resource
    MockMvc mockMvc;
    @Resource
    CompanyService companyService;

    @Resource
    EmployeeService employeeService;

    @Test
    void should_return_company_when_client_companies_with_post_given_url_and_company() throws Exception {
        //given
        String url = "/companies";
        Company company = new Company("boot");
        String companyJson = new ObjectMapper().writeValueAsString(company);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companyJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("boot"));
        company = companyService.getCompanies().get(0);
        Assertions.assertEquals("boot", company.getName());
        Assertions.assertEquals(1, company.getId());
    }

    @Test
    void should_return_companies_when_client_employees_with_get_given_url() throws Exception {
        //given
        String url = "/companies";
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(companyService.getCompanies().size())));
    }

    @Test
    void should_return_company_when_client_companies_and_id_with_get_given_url_and_id() throws Exception {
        //given
        String url = "/companies/1";
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("boot"));
    }

    @Test
    void should_return_companies_when_client_companies_and_with_get_given_url_and_page_and_size() throws Exception {
        //given
        String url = "/companies";
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get(url).param("page", String.valueOf(1)).param("size", String.valueOf(1)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(companyService.getCompanies(1, 1).size())));
    }

    @Test
    void should_return_employees_within_company_when_client_companies_and_with_get_given_url_and() throws Exception {
        //given
        String url = "/companies/1/employees";
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(employeeService.getEmployeesByCompanyId(1).size())));
    }

    @Test
    void should_update_company_when_client_companies_and_with_put_given_company() throws Exception {
        //given
        String url = "/companies/1";
        Company company = new Company("ctn");
        String companyJson = new ObjectMapper().writeValueAsString(company);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.put(url).contentType(MediaType.APPLICATION_JSON).content(companyJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("ctn"));
    }

    @Test
    void should_delete_company_when_client_companies_and_with_delete_given_company_id() throws Exception {
        //given
        String url = "/companies/1";
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.delete(url))
                .andExpect(MockMvcResultMatchers.status().isNoContent())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(employeeService.getEmployees().size())));
    }
}
