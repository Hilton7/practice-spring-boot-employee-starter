package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.bean.Company;
import com.thoughtworks.springbootemployee.bean.Employee;
import com.thoughtworks.springbootemployee.exception.EmployeeCreatedException;
import com.thoughtworks.springbootemployee.mapper.EmployeeMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EmployeeServiceTest {
    private EmployeeMapper employeeMapper = mock(EmployeeMapper.class);
    private EmployeeService employeeService = new EmployeeService(employeeMapper);

    @Test
    void should_return_created_employee_when_employee_age_is_between_18_to_65() {
        Employee employee = new Employee(null, "John", 20, "woman", null, 8000);
        when(employeeMapper.addEmployee(employee)).thenReturn(new Employee(1, "John", 20, "woman", null, 8000));
        Employee employeeResponse = employeeService.addEmployee(employee);
        Assertions.assertNotNull(employeeResponse);
        Assertions.assertEquals(employeeResponse.getId(), 1);
        Assertions.assertEquals(employeeResponse.getAge(), employee.getAge());
        Assertions.assertEquals(employeeResponse.getGender(), employee.getGender());
        Assertions.assertEquals(employeeResponse.getName(), employee.getName());
        Assertions.assertEquals(employeeResponse.getSalary(), employee.getSalary());
    }

    @ParameterizedTest
    @ValueSource(ints = {18, 65})
    void should_return_created_employee_when_employee_age_is_between_18_to_65_given_age_18_65(Integer age) {
        Employee employee = new Employee(null, "John", age, "woman", null, 8000);
        when(employeeMapper.addEmployee(employee)).thenReturn(new Employee(1, "John", age, "woman", null, 8000));
        Employee employeeResponse = employeeService.addEmployee(employee);
        Assertions.assertNotNull(employeeResponse);
        Assertions.assertEquals(employeeResponse.getId(), 1);
        Assertions.assertEquals(employeeResponse.getAge(), employee.getAge());
        Assertions.assertEquals(employeeResponse.getGender(), employee.getGender());
        Assertions.assertEquals(employeeResponse.getName(), employee.getName());
        Assertions.assertEquals(employeeResponse.getSalary(), employee.getSalary());
    }

    @ParameterizedTest
    @ValueSource(ints = {17, 66})
    void should_throw_exception_when_create_employee_given_employee_age_is_not_between_18_to_65(Integer age) {
        Employee employee = new Employee(null, "Marry", age, "female", null, 8000);
        EmployeeCreatedException employeeCreatedException = Assertions.assertThrows(EmployeeCreatedException.class, () -> employeeService.addEmployee(employee));
        Assertions.assertEquals(employeeCreatedException.getMessage(), "Employee must be 18-65 years old");
    }

    @Test
    void should_return_created_employee_when_create_employee_given_employee_age_is_30_and_salary_is_20000() {
        Employee employee = new Employee(null, "Marry", 30, "female", null, 20000);
        when(employeeMapper.addEmployee(employee)).thenReturn(new Employee(1, "Marry", 30, "female", null, 20000));
        Employee employeeResponse = employeeService.addEmployee(employee);
        Assertions.assertNotNull(employeeResponse);
        Assertions.assertEquals(employeeResponse.getId(), 1);
        Assertions.assertEquals(employeeResponse.getAge(), employee.getAge());
        Assertions.assertEquals(employeeResponse.getGender(), employee.getGender());
        Assertions.assertEquals(employeeResponse.getName(), employee.getName());
        Assertions.assertEquals(employeeResponse.getSalary(), employee.getSalary());
    }

    @ParameterizedTest(name = "{index} employee age is {0} and salary is {1}")
    @CsvSource({"30,20000", "29,19999"})
    void should_return_created_employee_when_create_employee_given_(Integer age, Integer salary) {
        Employee employee = new Employee(null, "Marry", age, "female", null, salary);
        when(employeeMapper.addEmployee(employee)).thenReturn(new Employee(1, "Marry", age, "female", null, salary));
        Employee employeeResponse = employeeService.addEmployee(employee);
        Assertions.assertNotNull(employeeResponse);
        Assertions.assertEquals(employeeResponse.getId(), 1);
        Assertions.assertEquals(employeeResponse.getAge(), employee.getAge());
        Assertions.assertEquals(employeeResponse.getGender(), employee.getGender());
        Assertions.assertEquals(employeeResponse.getName(), employee.getName());
        Assertions.assertEquals(employeeResponse.getSalary(), employee.getSalary());
    }

    @Test
    void should_throw_error_when_create_employee_given_salary_less_than_20000_and_age_morn_and_equal_30() {
        Employee employee = new Employee(null, "Marry", 30, "female", null, 19000);
        EmployeeCreatedException employeeCreatedException = Assertions.assertThrows(EmployeeCreatedException.class, () -> {
            employeeService.addEmployee(employee);
        });
        Assertions.assertEquals(employeeCreatedException.getMessage(), "Employee over 30 years old salary should morn than 20000 or equal 20000");
    }

    @Test
    void should_return_employee_status_is_1_when_create_employee() {
        Employee employee = new Employee(null, "John", 20, "woman", null, 8000);
        when(employeeMapper.addEmployee(employee)).thenReturn(new Employee(1, "John", 20, "woman", null, 8000));
        Employee employeeResponse = employeeService.addEmployee(employee);
        Assertions.assertNotNull(employeeResponse);
        Assertions.assertEquals(1, employeeResponse.getStatus());
    }


    @Test
    void should_return_employee_status_is_0_when_delete_employee() {
        Employee employee = new Employee(null, "John", 20, "woman", new Company(1, "root"), 8000);
        when(employeeMapper.deleteEmployeeById(1)).thenReturn(new Employee(1, "John", 20, "woman", new Company(1, "root"), 8000));
        Employee employeeResponse = employeeService.deleteEmployeeById(1);
        Assertions.assertNotNull(employeeResponse);
        Assertions.assertEquals(0, employeeResponse.getStatus());
        Assertions.assertEquals(employeeResponse.getId(), 1);
        Assertions.assertEquals(employeeResponse.getAge(), employee.getAge());
        Assertions.assertEquals(employeeResponse.getGender(), employee.getGender());
        Assertions.assertEquals(employeeResponse.getName(), employee.getName());
    }

    @Test
    void should_return_null_when_update_employee_and_employee_is_not_in_company() {
        when(employeeMapper.getEmployeeById(1)).thenReturn(new Employee(1, "John", 20, "woman", null, 8000));
        Employee employeeResponse = employeeService.updateEmployeeById(new Employee(null, "John", 22, "woman", null, 9000), 1);
        Assertions.assertNull(employeeResponse);
    }

    @Test
    void should_return_employee_when_update_employee_and_employee_is_in_company() {
        Employee employee = new Employee(1, "John", 20, "woman", new Company(1, "boot"), 8000);
        when(employeeMapper.updateEmployeeById(employee)).thenReturn(employee);
        Employee employeeResponse = employeeService.updateEmployeeById(employee, 1);
        Assertions.assertNotNull(employeeResponse);
        Assertions.assertEquals(1, employeeResponse.getStatus());
        Assertions.assertEquals(employeeResponse.getId(), employee.getId());
        Assertions.assertEquals(employeeResponse.getAge(), employee.getAge());
        Assertions.assertEquals(employeeResponse.getGender(), employee.getGender());
        Assertions.assertEquals(employeeResponse.getName(), employee.getName());
        Assertions.assertEquals(employeeResponse.getSalary(), employee.getSalary());
        Assertions.assertEquals(employeeResponse.getCompany().getId(), employee.getCompany().getId());
        Assertions.assertEquals(employeeResponse.getCompany().getName(), employee.getCompany().getName());
    }

    @Test
    void should_return_null_when_update_employee_and_employee_status_is_0() {
        Employee employee = new Employee(1, "John", 20, "woman", new Company(1, "boot"), 8000, 0);
        when(employeeMapper.updateEmployeeById(employee)).thenReturn(null);
        Employee employeeResponse = employeeService.updateEmployeeById(employee, 1);
        Assertions.assertNull(employeeResponse);
    }
}
