package com.thoughtworks.springbootemployee.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.bean.Employee;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private EmployeeService employeeService;
    @Autowired
    ObjectMapper mapper;

    @Test
    void should_call_service_when_perform_post_employee_given_() throws Exception {
        //given
        Employee employee = new Employee(null, "test", 18, "female", null, 20000);
        String json = mapper.writeValueAsString(employee);
        //when
        mockMvc.perform(post("/employees").contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isCreated());
        verify(employeeService).addEmployee(employee);
    }

    @Test
    void should_call_service_when_perform_delete_employee_given_() throws Exception {
        mockMvc.perform(delete("/employees/1")).andExpect(status().isNoContent());
        verify(employeeService).deleteEmployeeById(1);
    }

    @Test
    void should_call_service_when_perform_put_employee_given_() throws Exception {
        Employee employee = new Employee(null, "test", 18, "female", null, 20000);
        String json = mapper.writeValueAsString(employee);
        mockMvc.perform(put("/employees/1").contentType(MediaType.APPLICATION_JSON).content(json));
        verify(employeeService).updateEmployeeById(employee, 1);
    }
}
