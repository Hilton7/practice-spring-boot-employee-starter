package com.thoughtworks.springbootemployee;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.bean.Employee;
import com.thoughtworks.springbootemployee.mapper.CompanyMapper;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.annotation.Resource;

import static org.hamcrest.Matchers.hasSize;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeTest {
    @Resource
    CompanyMapper companyMapper;
    @Resource
    EmployeeService employeeService;
    @Resource
    MockMvc mockMvc;
    private final Employee EMPLOYEE = new Employee(null, "marry", 30, "woman", null, 800000);

    @BeforeEach
    public void setUp() {
        employeeService.addEmployee(EMPLOYEE);
    }

    @AfterEach
    public void cleanUp() {
        employeeService.clean();
    }

    @Test
    void should_return_employee_when_client_employees_with_post_given_url_and_employee() throws Exception {
        //given
        String url = "/employees";
        Employee employee = new Employee();
        employee.setSalary(1000);
        employee.setAge(18);
        employee.setName("Joe");
        employee.setGender("female");
        String employeeJson = new ObjectMapper().writeValueAsString(employee);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Joe"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").isNumber());
        employee = employeeService.getEmployeeById(2);
        Assertions.assertEquals("Joe", employee.getName());
        Assertions.assertEquals(18, employee.getAge());
        Assertions.assertEquals(1000, employee.getSalary());
        Assertions.assertEquals(2, employee.getId());
    }

    @Test
    void should_return_employees_when_client_employees_with_get_given_url() throws Exception {
        //given
        String url = "/employees";
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(employeeService.getEmployees().size())));
    }

    @Test
    void should_return_employees_when_client_employees_with_get_given_url_and_employee_gender() throws Exception {
        //given
        String url = "/employees";
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get(url).param("gender", "female"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(employeeService.getEmployeeByGender("female").size())));
    }

    @Test
    void should_update_employee_by_id_when_client_employees_with_put_given_url_and_employee_and_id() throws Exception {
        //given
        String url = "/employees/1";
        Employee employee = new Employee();
        employee.setSalary(1000);
        employee.setAge(18);
        String employeeJson = new ObjectMapper().writeValueAsString(employee);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.put(url).contentType(MediaType.APPLICATION_JSON).content(employeeJson))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(18))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(1000));
        employee = employeeService.getEmployeeById(1);
        Assertions.assertEquals(18, employee.getAge());
        Assertions.assertEquals(1000, employee.getSalary());
        Assertions.assertEquals(1, employee.getId());
    }

    @Test
    void should_delete_employee_by_id_when_client_employees_with_delete_given_url_and_id() throws Exception {
        //given
        String url = "/employees/1";
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.delete(url))
                .andExpect(MockMvcResultMatchers.status().isNoContent())
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(30))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(800000))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }


    @Test
    void should_return_employees_when_client_employees_with_get_given_url_and_page_size() throws Exception {
        //given
        String url = "/employees";
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get(url).param("page", String.valueOf(1)).param("size", String.valueOf(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(employeeService.getEmployees(1, 1).size())));
    }
}
