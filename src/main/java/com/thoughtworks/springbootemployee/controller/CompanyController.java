package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.bean.Company;
import com.thoughtworks.springbootemployee.bean.Employee;
import com.thoughtworks.springbootemployee.service.CompanyService;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class CompanyController {
    @Resource
    private CompanyService companyService;
    @Resource
    private EmployeeService employeeService;

    @GetMapping("/companies")
    public List<Company> getCompany() {
        return companyService.getCompanies();
    }

    @GetMapping("/companies/{id}")
    public Company getCompanyById(@PathVariable Integer id) {
        return companyService.getCompanyById(id);
    }

    @GetMapping("/companies/{id}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable Integer id) {
        return employeeService.getEmployeesByCompanyId(id);
    }

    @GetMapping(value = "/companies", params = {"page", "size"})
    public List<Company> getCompany(@RequestParam int page, @RequestParam int size) {
        return companyService.getCompanies(page, size);
    }

    @PostMapping("/companies")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Company addCompany(@RequestBody Company company) {
        return companyService.addCompany(company);
    }

    @PutMapping("/companies/{id}")
    public Company updateCompany(@RequestBody Company company, @PathVariable Integer id) {
        return companyService.updateCompany(id, company);
    }

    @DeleteMapping("/companies/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public List<Employee> deleteCompany(@PathVariable Integer id) {
        return companyService.deleteCompany(id);
    }
}
