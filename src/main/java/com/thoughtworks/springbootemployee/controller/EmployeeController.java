package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.bean.Employee;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class EmployeeController {

    @Resource
    private EmployeeService employeeService;

    @PostMapping("/employees")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Employee addEmployer(@RequestBody Employee employee) {
        return employeeService.addEmployee(employee);
    }


    @GetMapping("/employees")
    public List<Employee> getEmployees() {
        return employeeService.getEmployees();
    }

    @GetMapping("/employees/{id}")
    public Employee getEmployeeById(@PathVariable int id) {
        return employeeService.getEmployeeById(id);
    }

    @GetMapping(value = "/employees", params = "gender")
    public List<Employee> getEmployeeByGender(@RequestParam("gender") String gender) {
        return employeeService.getEmployeeByGender(gender);
    }

    @PutMapping(value = "/employees/{id}")
    public Employee changeEmployee(@RequestBody Employee employee, @PathVariable int id) {
        return employeeService.updateEmployeeById(employee, id);
    }

    @DeleteMapping(value = "/employees/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Employee deleteEmployee(@PathVariable("id") Integer id) {
        return employeeService.deleteEmployeeById(id);
    }

    @GetMapping(value = "/employees", params = {"page", "size"})
    public List<Employee> getEmployees(@RequestParam("page") int page, @RequestParam("size") int size) {
        return employeeService.getEmployees(page, size);
    }
}
