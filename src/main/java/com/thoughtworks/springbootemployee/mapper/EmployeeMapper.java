package com.thoughtworks.springbootemployee.mapper;

import com.thoughtworks.springbootemployee.bean.Employee;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class EmployeeMapper {
    private List<Employee> employees = new ArrayList<Employee>();
    @Resource
    private CompanyMapper companyMapper;

    public Employee addEmployee(Employee employee) {
        employee.setId(generateId());
        employees.add(employee);
        return employee;
    }

    public Employee updateEmployeeById(Employee employee) {
        List<Employee> employeesList = employees.stream()
                .filter(em -> em.getId().equals(employee.getId()))
                .collect(Collectors.toList());
        if (employeesList == null || employeesList.size() == 0) {
            return null;
        }
        Employee updateEmployee = employeesList.get(0);
        if (updateEmployee.getStatus() == 0)
            return null;
        if (employee.getAge() != null)
            updateEmployee.setAge(employee.getAge());
        if (employee.getSalary() != null)
            updateEmployee.setSalary(employee.getSalary());
        if (employee.getCompany() != null)
            updateEmployee.setCompany(employee.getCompany());
        return updateEmployee;
    }


    public Employee deleteEmployeeById(Integer id) {
        if (id == null) return null;
        return getEmployeeById(id);
    }

    public List<Employee> getEmployees(int page, int size) {
        List<Employee> result = null;
        try {
            int startNum = (page - 1) * size;
            if (startNum + size >= employees.size()) {
                result = employees.subList(startNum, employees.size());
            } else {
                result = employees.subList(startNum, startNum + size);
            }
        } catch (Exception e) {
            return null;
        }
        return result;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public List<Employee> getEmployeeByGender(String gender) {
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getGender(), gender))
                .collect(Collectors.toList());
    }

    public Employee getEmployeeById(int id) {
        return employees.stream()
                .filter(employee -> employee.getId() == id)
                .findFirst().orElse(null);
    }

    public Integer generateId() {
        if (employees.size() == 0) {
            return 1;
        }
        return (employees.stream().max(Comparator.comparing(Employee::getId)).get().getId() + 1);
    }

    public List<Employee> getEmployeesByCompanyId(int id) {
        return employees.stream()
                .filter(employee -> employee.getCompany().getId().equals(id))
                .collect(Collectors.toList());
    }

    public List<Employee> deleteByCompanyId(int id) {
        List<Employee> employeeList = employees.stream()
                .filter(employee -> employee.getCompany().getId().equals(id))
                .collect(Collectors.toList());
        return employeeList;
    }

    public void clean() {
        employees.clear();
    }
}
