package com.thoughtworks.springbootemployee.mapper;

import com.thoughtworks.springbootemployee.bean.Company;
import com.thoughtworks.springbootemployee.bean.Employee;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CompanyMapper {
    private List<Company> companies = new ArrayList<Company>();
    @Resource
    private EmployeeMapper employeeMapper;

    public Company addCompany(Company company) {
        if (!isIn(company)) {
            if (company.getId() == null) {
                company.setId(generateId());
            }
            companies.add(company);
        }
        return company;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public Company getCompanyById(int id) {
        Company resultCompany = null;
        try {
            resultCompany = companies.stream()
                    .filter(company -> company.getId().equals(id))
                    .collect(Collectors.toList())
                    .get(0);
        } catch (Exception e) {
            return null;
        }
        return resultCompany;
    }


    public List<Company> getCompanies(int page, int size) {
        List<Company> result = null;
        try {
            int startNum = (page - 1) * size;
            if (startNum + size >= companies.size()) {
                result = companies.subList(startNum, companies.size());
            } else {
                result = companies.subList(startNum, startNum + size);
            }
        } catch (Exception e) {
            return null;
        }
        return result;
    }

    public Company updateCompany(int id, Company company) {
        List<Company> collect = companies.stream().filter(cy -> cy.getId().equals(id)).collect(Collectors.toList());
        if (collect == null || collect.size() == 0) {
            return null;
        }
        Company updateCompany = collect.get(0);
        return updateCompany;
    }


    public List<Employee> deleteCompany(int id) {
        List<Company> collect = companies.stream().filter(cy -> cy.getId().equals(id)).collect(Collectors.toList());
        if (collect == null || collect.size() == 0) {
            return null;
        }
        Company deleteCompany = collect.get(0);
        companies.remove(deleteCompany);
        return employeeMapper.deleteByCompanyId(deleteCompany.getId());
    }

    public boolean isIn(Company company) {
        for (Company cy : companies) {
            if (cy.equals(company)) return true;
        }
        return false;
    }


    public Integer generateId() {
        if (companies.size() == 0) {
            return 1;
        }
        return (companies.stream().max(Comparator.comparing(Company::getId)).get().getId() + 1);
    }

    public void clean() {
        this.companies.clear();
    }
}
