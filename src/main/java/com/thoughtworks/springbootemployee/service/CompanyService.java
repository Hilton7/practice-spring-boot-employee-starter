package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.bean.Company;
import com.thoughtworks.springbootemployee.bean.Employee;
import com.thoughtworks.springbootemployee.mapper.CompanyMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

@Service
public class CompanyService {
    @Resource
    private CompanyMapper companyMapper;

    public Company addCompany(Company company) {
        return companyMapper.addCompany(company);
    }

    public List<Company> getCompanies() {
        return companyMapper.getCompanies();
    }

    public Company getCompanyById(Integer id) {
        if (id == null) {
            return null;
        }
        return companyMapper.getCompanyById(id);
    }

    public List<Company> getCompanies(int page, int size) {
        return companyMapper.getCompanies(page, size);
    }

    public Company updateCompany(Integer id, Company company) {
        if (id == null || Objects.isNull(company)) {
            return null;
        }
        Company updateCompany = companyMapper.updateCompany(id, company);
        updateCompany.setName(company.getName());
        return updateCompany;
    }

    public List<Employee> deleteCompany(Integer id) {
        if (id == null)
            return null;
        List<Employee> employeeList = companyMapper.deleteCompany(id);
        employeeList.forEach(employee -> employee.setStatus(0));
        return employeeList;
    }

    public void clean() {
        companyMapper.clean();
    }
}
