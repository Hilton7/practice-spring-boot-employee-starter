package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.bean.Employee;
import com.thoughtworks.springbootemployee.exception.EmployeeCreatedException;
import com.thoughtworks.springbootemployee.mapper.CompanyMapper;
import com.thoughtworks.springbootemployee.mapper.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class EmployeeService {
    @Resource
    private EmployeeMapper employeeMapper;
    @Resource
    private CompanyMapper companyMapper;
    private static final String EMPLOYEE_AGE_RANGE_ERROR_MESSAGE = "Employee must be 18-65 years old";
    private static final String EMPLOYEE_AND_SALARY_ERROR_MESSAGE = "Employee over 30 years old salary should morn than 20000 or equal 20000";

    @Autowired
    public EmployeeService(EmployeeMapper employeeMapper) {
        this.employeeMapper = employeeMapper;
    }

    public Employee addEmployee(Employee employee) {
        isLegalAgeAndSalary(employee.getAge(), employee.getSalary());
        if (employee.getCompany() != null && employee.getCompany().getId() != null)
            companyMapper.addCompany(employee.getCompany());
        return employeeMapper.addEmployee(employee);
    }

    public void isLegalAgeAndSalary(int age, int salary) {
        if (age < 18 || age > 65 || age < 0)
            throw new EmployeeCreatedException(EMPLOYEE_AGE_RANGE_ERROR_MESSAGE);
        if (age >= 30 && salary < 20000)
            throw new EmployeeCreatedException(EMPLOYEE_AND_SALARY_ERROR_MESSAGE);
    }

    public Employee deleteEmployeeById(int id) {
        Employee employee = employeeMapper.deleteEmployeeById(id);
        employee.setStatus(0);
        return employee;
    }

    public Employee updateEmployeeById(Employee employee, int id) {
        employee.setId(id);
        return employeeMapper.updateEmployeeById(employee);
    }

    public List<Employee> getEmployeesByCompanyId(int id) {
        return employeeMapper.getEmployeesByCompanyId(id);
    }

    public List<Employee> getEmployees() {
        return employeeMapper.getEmployees();
    }

    public List<Employee> getEmployeeByGender(String female) {
        return employeeMapper.getEmployeeByGender(female);
    }

    public Employee getEmployeeById(Integer id) {
        return employeeMapper.getEmployeeById(id);
    }

    public List<Employee> getEmployees(int page, int size) {
        return employeeMapper.getEmployees(page, size);
    }

    public void clean() {
        employeeMapper.clean();
    }
}
