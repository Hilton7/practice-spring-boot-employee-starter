package com.thoughtworks.springbootemployee.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Company {
    List<Employee> employees = new ArrayList<Employee>();
    private Integer id;
    private String name;

    public Company(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Company(String name) {
        this.name = name;
    }

    public Company() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (Objects.isNull(obj) || !(obj instanceof Company)
                || Objects.isNull(((Company) obj).getId()) || Objects.isNull(this.id)) {
            return false;
        }
        Company company = (Company) obj;
        return company.getId().equals(this.id);
    }
}
